#!/bin/sh

# Create a single init file for profiling
cat ~/.emacs.d/custom.el ~/.emacs.d/config.el ~/.emacs.d/my-inits/*.el > ~/.emacs.d/profile-concat.el

# Start Emacs
emacs -Q -l ~/.emacs.d/my-extensions/profile-dotemacs/profile-dotemacs.el -f profile-dotemacs

# Delete the concatenated init file
rm -f ~/.emacs.d/profile-concat.el
