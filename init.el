;; Don't let Emacs write customization variables in this file
;; ----------------------------------------------------------

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;(package-initialize)

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file)
)
;(load custom-file)


;; Load init scripts configuration parameters
;; ------------------------------------------
(load (expand-file-name "config" user-emacs-directory))


;; Start init-loader to support fragmentated initialization scripts
;; ----------------------------------------------------------------
(add-to-list 'load-path (expand-file-name "my-extensions/init-loader" user-emacs-directory))
(require 'init-loader)
(setq init-loader-show-log-after-init nil)
(init-loader-load (expand-file-name "my-inits" user-emacs-directory))
