;; TODO: Support Cscope

(deftheme nicedark-term
  "Dark theme with green decorations.
Supports general Emacs appearance, Font Lock, Hi Lock, Terminal, ECB, Speedbar, ANSI colors.")

(let ((class '((class color) (min-colors 89))))
  (custom-theme-set-faces
   'nicedark-term

   ;; Emacs appearance and features
   ;; -----------------------------

   ;; Header line
   ;; 'header-line', 'mode-line-inactive' are identical
   `(header-line ((,class (:foreground "#80a180" :background "#202020" :box (:line-width -1 :color "#80a180")))))

   ;; Mode line
   ;; 'mode-line', 'speedbar-separator-face' are identical
   `(mode-line ((,class (:foreground "black" :background "#80a180" :box nil))))
   ;; 'header-line', 'mode-line-inactive' are identical
   `(mode-line-inactive ((,class (:foreground "#80a180" :background "#202020" :box nil))))
   `(mode-line-highlight ((,class (:foreground "#80a180" :background "#212526" :box (:line-width -1 :color "#80a180")))))

   ;; Minibuffer
   `(minibuffer-prompt ((,class (:foreground "#f78585"))))

   ;; Window borders
   `(fringe ((,class (:foreground "#909090" :background "#212526"))))
   `(linum ((,class (:foreground "#909090" :background "#212526"))))

   ;; Vertical window separator
   `(vertical-border ((,class (:foreground "#202020"))))

   ;; Tool tips
   `(tooltip ((,class (:foreground "#212526" :background "#80a180"))))

   ;; Buttons and links
   ;; 'link' and 'button' are identical
   `(link ((,class (:foreground "#7096c2" :underline nil))))
   ;; 'link-visited', 'speedbar-selected-face' are identical
   `(link-visited ((,class (:foreground "#907397" :underline nil))))
   ;; 'link' and 'button' are identical
   `(button ((,class (:foreground "#7096c2" :underline nil))))
   ;; 'highlight' and 'speedbar-highlight-face' are identical
   `(highlight ((,class (:foreground "#7096c2" :background "#212526" :underline t))))
   ;; TODO: Add 'custom-button-unraised' and 'custom-button-pressed-unraised'?
   ;; Blue variant:
   `(custom-button ((,class (:foreground "#7096c2" :background "#263744" :box (:line-width -1 :color "#263744")))))
   `(custom-button-mouse ((,class (:foreground "#263744" :background "#7096c2" :box (:line-width -1 :color "#7096c2")))))
   `(custom-button-pressed ((,class (:foreground "#7096c2" :background "#263744" :box (:line-width -1 :color "#7096c2")))))

   ;; Default face
   ;; 'default' and 'info-menu-star' are identical
   `(default ((,class (:foreground "#d0d0d0" :background "#212526"))))

   ;; Cursor
   `(cursor ((,class (:background "#80a180"))))

   ;; Mouse selection
   `(region ((,class (:background "#263744"))))

   ;; Parenthesis matching
   `(show-paren-match ((,class (:background "#2e3d3d"))))
   `(show-paren-mismatch ((,class (:foreground "#f78585" :background "#854747" :bold t))))

   ;; Buffer Menu
   `(buffer-menu-buffer ((,class (:inherit link))))

   ;; I-search
   ;; Because 'isearch' attributes are added over the 'lazy-highlight' face,
   ;; it is impossible to keep the original foreground when lazy-highlight's
   ;; foreground color is set.
   ;; 'isearch' and 'hi-yellow' are identical (except for :bold)
   `(isearch ((,class (:foreground "#212526" :background "#cfcf99" :bold t))))
   `(lazy-highlight ((,class (:foreground "#cfcf99" :background "#373d40" :bold t))))

   ;; Escaped characters
   `(escape-glyph ((,class (:foreground "#f78585"))))

   ;; Customize
   `(custom-group-tag ((,class (:foreground "#f78585"))))
   `(custom-variable-tag ((,class (:foreground "#c99563"))))
   `(custom-state ((,class (:foreground "#8fb28f"))))

   ;; Warnings and errors
   `(warning ((,class (:foreground "#c99563"))))
   `(error ((,class (:foreground "#f78585"))))

   ;; Compilation faces
   `(compilation-info ((,class (:inherit link))))
   `(compilation-mode-line-exit ((,class (:inherit compilation-info :foreground nil))))
   `(compilation-mode-line-fail ((,class (:inherit compilation-error :foreground nil))))

   ;; Font Lock
   ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html
   `(font-lock-builtin-face ((,class (:foreground "#f78585"))))
   `(font-lock-comment-face ((,class (:foreground "#909090"))))
   `(font-lock-constant-face ((,class (:foreground "#7096c2"))))
   `(font-lock-doc-face ((,class (:foreground "#907397"))))
   `(font-lock-function-name-face ((,class (:foreground "#7096c2" :bold t))))
   `(font-lock-keyword-face ((,class (:foreground "#f78585" :bold t))))
   `(font-lock-negation-char-face ((,class (:foreground "#f78585" :bold t))))
   `(font-lock-preprocessor-face ((,class (:foreground "#c99563"))))
   ;; TODO: Add 'Font Lock Regexp Grouping Backslash' and 'Font Lock Regexp Grouping Construct'?
   `(font-lock-string-face ((,class (:foreground "#8fb28f"))))
   `(font-lock-type-face ((,class (:foreground "#e6e6aa" :bold t))))
   `(font-lock-variable-name-face ((,class (:foreground "#e6e6aa"))))
   `(font-lock-warning-face ((,class (:foreground "#f78585" :bold t))))

   ;; Hi Lock
   ;; 'hi-blue', 'ecb-tag-header-face' are identical
   `(hi-blue ((,class (:foreground "#d0d0d0" :background "#263744" :bold t))))
   ;; 'lazy-highlight' and 'hi-yellow' are identical (except for :bold)
   `(hi-yellow ((,class (:foreground "#212526" :background "#cfcf99"))))

   ;; Terminal
   ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Terminal-emulator.html
   `(term-color-black   ((,class (:foreground "#202020" :background "#202020"))))
   `(term-color-red     ((,class (:foreground "#f78585" :background "#f78585"))))
   `(term-color-green   ((,class (:foreground "#80a180" :background "#80a180"))))
   `(term-color-yellow  ((,class (:foreground "#e6e6aa" :background "#e6e6aa"))))
   `(term-color-blue    ((,class (:foreground "#7096c2" :background "#7096c2"))))
   `(term-color-magenta ((,class (:foreground "#907397" :background "#907397"))))
   `(term-color-cyan    ((,class (:foreground "#499da9" :background "#499da9"))))
   `(term-color-white   ((,class (:foreground "#d0d0d0" :background "#d0d0d0"))))

   ;; Ido (Interactively Do Things)
   ;; 'ido-subdir' and 'speedbar-directory-face' are identical
   `(ido-subdir ((,class (:foreground "#7096c2"))))
   ;; 'ido-first-match' and 'ido-only-match' are identical
   `(ido-first-match ((,class (:foreground "#e6e6aa" :bold t))))
   `(ido-only-match ((,class (:foreground "#e6e6aa" :bold t))))
   `(ido-indicator ((,class (:foreground "#202020" :background "#f78585"))))
   ;; 'ido-incomplete-regexp' inherits from Font Lock by default
   ;; 'ido-virtual' inherits from Font Lock by default

   ;; Grep
   `(match ((,class (:foreground "#e6e6aa" :background "#212526" :bold t))))
   ;; foreground color may be changed to red to match ggtags colors
   ;; ggtags color for the match is based on the ANSI color map and
   ;; cannot be customized individually

   ;; Misc
   ;; 'default' and 'info-menu-star' are identical
   `(info-menu-star ((,class (:foreground "#d0d0d0" :background "#212526"))))
   `(trailing-whitespace ((,class (:background "#f78585"))))


   ;; Whitespace mode
   ;; ---------------
   `(whitespace-big-indent ((,class (:background "#f78585"))))
   `(whitespace-empty ((,class (:background "#f78585"))))
   `(whitespace-hspace ((,class (:background "#f78585"))))
   `(whitespace-indentation ((,class (:background "#f78585"))))
   `(whitespace-line ((,class (:foreground "#f78585" :background "#373d40"))))
   `(whitespace-newline ((,class (:background "#f78585"))))
   `(whitespace-space ((,class (:background "#f78585"))))
   `(whitespace-space-after-tab ((,class (:background "#f78585"))))
   `(whitespace-space-before-tab ((,class (:background "#f78585"))))
   `(whitespace-tab ((,class (:background "#f78585"))))
   `(whitespace-trailing ((,class (:background "#f78585"))))


   ;; ECB (Emacs Code Browser)
   ;; ------------------------
   ;; http://ecb.sourceforge.net/docs/ecb-faces.html
   `(ecb-default-general-face ((,class (:foreground "#d0d0d0"))))
   `(ecb-default-highlight-face ((,class (:foreground "black" :background "#80a180"))))
   ;; 'hi-blue', 'ecb-tag-header-face' are identical
   `(ecb-tag-header-face ((,class (:foreground "#d0d0d0" :background "#263744" :bold t))))


   ;; Speedbar
   ;; --------
   ;; https://www.gnu.org/software/emacs/manual/html_node/speedbar/Frames-and-Faces.html
   `(speedbar-button-face ((,class (:foreground "#d0d0d0"))))
   `(speedbar-file-face ((,class (:foreground "#d0d0d0"))))
   `(speedbar-directory-face ((,class (:foreground "#7096c2"))))
   `(speedbar-tag-face ((,class (:foreground "#e6e6aa"))))
   `(speedbar-selected-face ((,class (:foreground "#e6e6aa" :underline nil))))
   ;; 'highlight' and 'speedbar-highlight-face' are identical
   `(speedbar-highlight-face ((,class (:foreground "#7096c2" :background "#212526" :underline t))))
   ;; 'mode-line', 'speedbar-separator-face' are identical (apart from the overline forcibly disabled here)
   `(speedbar-separator-face ((,class (:foreground "black" :background "#80a180" :box nil :overline nil))))


   ;; Ggtags
   ;; ------
   `(ggtags-global-line ((,class (:background "#263744"))))
   ;; TODO: Define 'ggtags-highlight' to customize "highlight tag at point"
   ;; `(ggtags-highlight ((,class ())))


   ;; Markdown
   ;; --------
   ;; Most faces inherit from Font Lock faces and are just fine
   `(markdown-link-face ((,class (:inherit link-visited :bold t))))
   `(markdown-url-face ((,class (:inherit link-visited))))
   `(markdown-header-face-1 ((,class (:foreground "#f78585"))))
   `(markdown-header-face-2 ((,class (:foreground "#c99563"))))
   `(markdown-header-face-3 ((,class (:foreground "#e6e6aa"))))
   `(markdown-list-face ((,class (:foreground "#c99563"))))
   `(markdown-code-face ((,class (:inherit font-lock-constant-face))))


   ;; Sh-Script
   ;; ---------
   `(sh-heredoc ((,class (:inherit font-lock-constant-face))))

   ;; ReST
   ;; ----
   ;; Different shades of #f78585,
   ;; as suggested on https://www.color-hex.com/color/f78585
   `(rst-level-1 ((,class (:background "#c56a6a"))))
   `(rst-level-2 ((,class (:background "#ac5d5d"))))
   `(rst-level-3 ((,class (:background "#944f4f"))))
   `(rst-level-4 ((,class (:background "#7b4242"))))
   `(rst-level-5 ((,class (:background "#623535"))))
   `(rst-level-6 ((,class (:background "#4a2727"))))
  ))

(custom-theme-set-variables
 'nicedark-term

 ;; ansi-color-names-vector color order:
 ;;   black, red, green, yellow,
 ;;   blue, magenta, cyan, white
 '(ansi-color-names-vector ["#212526" "#f78585" "#8fb28f" "#e6e6aa"
                            "#7096c2" "#907397" "#499da9" "#d0d0d0"])
 )

(provide-theme 'nicedark-term)
