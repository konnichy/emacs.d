#!/usr/bin/python

import sys
import os
import re
import shutil

colormap = {"MYWHITE":"#d0d0d0",  # RGB 208, 208, 208
            "MY_GRAY":"#909090",  # RGB 144, 144, 144
            "MYDGRAY":"#202020",  # RGB  32,  32,  32
            "MY__RED":"#f78585",  # RGB 245, 133, 133
            "MY_BGRN":"#8fb28f",  # RGB 143, 178, 143
            "MYGREEN":"#80a180",  # RGB 128, 161, 128
            "MYBKGND":"#21282b",
            "BKGNDT1":"#373d40",  # Tint 1 - https://www.color-hex.com/color/21282b
            "MY_BLUE":"#7096c2",  # RGB 112, 150, 194
            "MYPRPLE":"#907397",  # RGB 144, 115, 151
            "MYYELOW":"#e6e6aa",
            "MYYELS2":"#cfcf99",  # Shade 2 - https://www.color-hex.com/color/e6e6aa
            "MYORNGE":"#c99563",  # RGB 201, 149,  99
            "MY_CYAN":"#499da9",  # RGB  73, 157, 169
            "MY_TRED":"#854747",
            "MY_TGRN":"#2e3d3d",  # RGB  46,  61,  61
            "MY_TBLU":"#263744"}  # RGB  38,  55,  68

termmap = {"nicedark":"nicedark-term",
           "MYBKGND":"#212526"}

# Source: http://stackoverflow.com/questions/4934806/how-can-i-find-scripts-directory-with-python
source_directory = os.path.dirname(os.path.realpath(sys.argv[0]))

# Source: http://stackoverflow.com/questions/10918682/concatenate-path-platform-independent
with open(os.path.join(source_directory, "nicedark-template.el"), "r") as template:
    template_lines = template.readlines()

with open(os.path.join(source_directory, "nicedark-theme.el"), "w") as theme:
    # Source: http://stackoverflow.com/questions/2400504/easiest-way-to-replace-a-string-using-a-dictionary-of-replacements
    pattern = re.compile('|'.join(colormap.keys()))
    for template_line in template_lines:
        theme_line = pattern.sub(lambda x: colormap[x.group()], template_line)
        theme.write(theme_line)

with open(os.path.join(source_directory, "nicedark-term-theme.el"), "w") as theme:
    # Source: http://stackoverflow.com/questions/2400504/easiest-way-to-replace-a-string-using-a-dictionary-of-replacements
    color_pattern = re.compile('|'.join(colormap.keys()))
    term_pattern = re.compile('|'.join(termmap.keys()))
    for template_line in template_lines:
        theme_line = term_pattern.sub(lambda x: termmap[x.group()], template_line)
        theme_line = color_pattern.sub(lambda x: colormap[x.group()], theme_line)
        theme.write(theme_line)
