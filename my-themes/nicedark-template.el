;; TODO: Support Cscope

(deftheme nicedark
  "Dark theme with green decorations.
Supports general Emacs appearance, Font Lock, Hi Lock, Terminal, ECB, Speedbar, ANSI colors.")

(let ((class '((class color) (min-colors 89))))
  (custom-theme-set-faces
   'nicedark

   ;; Emacs appearance and features
   ;; -----------------------------

   ;; Header line
   ;; 'header-line', 'mode-line-inactive' are identical
   `(header-line ((,class (:foreground "MYGREEN" :background "MYDGRAY" :box (:line-width -1 :color "MYGREEN")))))

   ;; Mode line
   ;; 'mode-line', 'speedbar-separator-face' are identical
   `(mode-line ((,class (:foreground "black" :background "MYGREEN" :box nil))))
   ;; 'header-line', 'mode-line-inactive' are identical
   `(mode-line-inactive ((,class (:foreground "MYGREEN" :background "MYDGRAY" :box nil))))
   `(mode-line-highlight ((,class (:foreground "MYGREEN" :background "MYBKGND" :box (:line-width -1 :color "MYGREEN")))))

   ;; Minibuffer
   `(minibuffer-prompt ((,class (:foreground "MY__RED"))))

   ;; Window borders
   `(fringe ((,class (:foreground "MY_GRAY" :background "MYBKGND"))))
   `(linum ((,class (:foreground "MY_GRAY" :background "MYBKGND"))))

   ;; Vertical window separator
   `(vertical-border ((,class (:foreground "MYDGRAY"))))

   ;; Tool tips
   `(tooltip ((,class (:foreground "MYBKGND" :background "MYGREEN"))))

   ;; Buttons and links
   ;; 'link' and 'button' are identical
   `(link ((,class (:foreground "MY_BLUE" :underline nil))))
   ;; 'link-visited', 'speedbar-selected-face' are identical
   `(link-visited ((,class (:foreground "MYPRPLE" :underline nil))))
   ;; 'link' and 'button' are identical
   `(button ((,class (:foreground "MY_BLUE" :underline nil))))
   ;; 'highlight' and 'speedbar-highlight-face' are identical
   `(highlight ((,class (:foreground "MY_BLUE" :background "MYBKGND" :underline t))))
   ;; TODO: Add 'custom-button-unraised' and 'custom-button-pressed-unraised'?
   ;; Blue variant:
   `(custom-button ((,class (:foreground "MY_BLUE" :background "MY_TBLU" :box (:line-width -1 :color "MY_TBLU")))))
   `(custom-button-mouse ((,class (:foreground "MY_TBLU" :background "MY_BLUE" :box (:line-width -1 :color "MY_BLUE")))))
   `(custom-button-pressed ((,class (:foreground "MY_BLUE" :background "MY_TBLU" :box (:line-width -1 :color "MY_BLUE")))))

   ;; Default face
   ;; 'default' and 'info-menu-star' are identical
   `(default ((,class (:foreground "MYWHITE" :background "MYBKGND"))))

   ;; Cursor
   `(cursor ((,class (:background "MYGREEN"))))

   ;; Mouse selection
   `(region ((,class (:background "MY_TBLU"))))

   ;; Parenthesis matching
   `(show-paren-match ((,class (:background "MY_TGRN"))))
   `(show-paren-mismatch ((,class (:foreground "MY__RED" :background "MY_TRED" :bold t))))

   ;; Buffer Menu
   `(buffer-menu-buffer ((,class (:inherit link))))

   ;; I-search
   ;; Because 'isearch' attributes are added over the 'lazy-highlight' face,
   ;; it is impossible to keep the original foreground when lazy-highlight's
   ;; foreground color is set.
   ;; 'isearch' and 'hi-yellow' are identical (except for :bold)
   `(isearch ((,class (:foreground "MYBKGND" :background "MYYELS2" :bold t))))
   `(lazy-highlight ((,class (:foreground "MYYELS2" :background "BKGNDT1" :bold t))))

   ;; Escaped characters
   `(escape-glyph ((,class (:foreground "MY__RED"))))

   ;; Customize
   `(custom-group-tag ((,class (:foreground "MY__RED"))))
   `(custom-variable-tag ((,class (:foreground "MYORNGE"))))
   `(custom-state ((,class (:foreground "MY_BGRN"))))

   ;; Warnings and errors
   `(warning ((,class (:foreground "MYORNGE"))))
   `(error ((,class (:foreground "MY__RED"))))

   ;; Compilation faces
   `(compilation-info ((,class (:inherit link))))
   `(compilation-mode-line-exit ((,class (:inherit compilation-info :foreground nil))))
   `(compilation-mode-line-fail ((,class (:inherit compilation-error :foreground nil))))

   ;; Font Lock
   ;; https://www.gnu.org/software/emacs/manual/html_node/elisp/Faces-for-Font-Lock.html
   `(font-lock-builtin-face ((,class (:foreground "MY__RED"))))
   `(font-lock-comment-face ((,class (:foreground "MY_GRAY"))))
   `(font-lock-constant-face ((,class (:foreground "MY_BLUE"))))
   `(font-lock-doc-face ((,class (:foreground "MYPRPLE"))))
   `(font-lock-function-name-face ((,class (:foreground "MY_BLUE" :bold t))))
   `(font-lock-keyword-face ((,class (:foreground "MY__RED" :bold t))))
   `(font-lock-negation-char-face ((,class (:foreground "MY__RED" :bold t))))
   `(font-lock-preprocessor-face ((,class (:foreground "MYORNGE"))))
   ;; TODO: Add 'Font Lock Regexp Grouping Backslash' and 'Font Lock Regexp Grouping Construct'?
   `(font-lock-string-face ((,class (:foreground "MY_BGRN"))))
   `(font-lock-type-face ((,class (:foreground "MYYELOW" :bold t))))
   `(font-lock-variable-name-face ((,class (:foreground "MYYELOW"))))
   `(font-lock-warning-face ((,class (:foreground "MY__RED" :bold t))))

   ;; Hi Lock
   ;; 'hi-blue', 'ecb-tag-header-face' are identical
   `(hi-blue ((,class (:foreground "MYWHITE" :background "MY_TBLU" :bold t))))
   ;; 'lazy-highlight' and 'hi-yellow' are identical (except for :bold)
   `(hi-yellow ((,class (:foreground "MYBKGND" :background "MYYELS2"))))

   ;; Terminal
   ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Terminal-emulator.html
   `(term-color-black   ((,class (:foreground "MYDGRAY" :background "MYDGRAY"))))
   `(term-color-red     ((,class (:foreground "MY__RED" :background "MY__RED"))))
   `(term-color-green   ((,class (:foreground "MYGREEN" :background "MYGREEN"))))
   `(term-color-yellow  ((,class (:foreground "MYYELOW" :background "MYYELOW"))))
   `(term-color-blue    ((,class (:foreground "MY_BLUE" :background "MY_BLUE"))))
   `(term-color-magenta ((,class (:foreground "MYPRPLE" :background "MYPRPLE"))))
   `(term-color-cyan    ((,class (:foreground "MY_CYAN" :background "MY_CYAN"))))
   `(term-color-white   ((,class (:foreground "MYWHITE" :background "MYWHITE"))))

   ;; Ido (Interactively Do Things)
   ;; 'ido-subdir' and 'speedbar-directory-face' are identical
   `(ido-subdir ((,class (:foreground "MY_BLUE"))))
   ;; 'ido-first-match' and 'ido-only-match' are identical
   `(ido-first-match ((,class (:foreground "MYYELOW" :bold t))))
   `(ido-only-match ((,class (:foreground "MYYELOW" :bold t))))
   `(ido-indicator ((,class (:foreground "MYDGRAY" :background "MY__RED"))))
   ;; 'ido-incomplete-regexp' inherits from Font Lock by default
   ;; 'ido-virtual' inherits from Font Lock by default

   ;; Grep
   `(match ((,class (:foreground "MYYELOW" :background "MYBKGND" :bold t))))
   ;; foreground color may be changed to red to match ggtags colors
   ;; ggtags color for the match is based on the ANSI color map and
   ;; cannot be customized individually

   ;; Misc
   ;; 'default' and 'info-menu-star' are identical
   `(info-menu-star ((,class (:foreground "MYWHITE" :background "MYBKGND"))))
   `(trailing-whitespace ((,class (:background "MY__RED"))))


   ;; Whitespace mode
   ;; ---------------
   `(whitespace-big-indent ((,class (:background "MY__RED"))))
   `(whitespace-empty ((,class (:background "MY__RED"))))
   `(whitespace-hspace ((,class (:background "MY__RED"))))
   `(whitespace-indentation ((,class (:background "MY__RED"))))
   `(whitespace-line ((,class (:foreground "MY__RED" :background "BKGNDT1"))))
   `(whitespace-newline ((,class (:background "MY__RED"))))
   `(whitespace-space ((,class (:background "MY__RED"))))
   `(whitespace-space-after-tab ((,class (:background "MY__RED"))))
   `(whitespace-space-before-tab ((,class (:background "MY__RED"))))
   `(whitespace-tab ((,class (:background "MY__RED"))))
   `(whitespace-trailing ((,class (:background "MY__RED"))))


   ;; ECB (Emacs Code Browser)
   ;; ------------------------
   ;; http://ecb.sourceforge.net/docs/ecb-faces.html
   `(ecb-default-general-face ((,class (:foreground "MYWHITE"))))
   `(ecb-default-highlight-face ((,class (:foreground "black" :background "MYGREEN"))))
   ;; 'hi-blue', 'ecb-tag-header-face' are identical
   `(ecb-tag-header-face ((,class (:foreground "MYWHITE" :background "MY_TBLU" :bold t))))


   ;; Speedbar
   ;; --------
   ;; https://www.gnu.org/software/emacs/manual/html_node/speedbar/Frames-and-Faces.html
   `(speedbar-button-face ((,class (:foreground "MYWHITE"))))
   `(speedbar-file-face ((,class (:foreground "MYWHITE"))))
   `(speedbar-directory-face ((,class (:foreground "MY_BLUE"))))
   `(speedbar-tag-face ((,class (:foreground "MYYELOW"))))
   `(speedbar-selected-face ((,class (:foreground "MYYELOW" :underline nil))))
   ;; 'highlight' and 'speedbar-highlight-face' are identical
   `(speedbar-highlight-face ((,class (:foreground "MY_BLUE" :background "MYBKGND" :underline t))))
   ;; 'mode-line', 'speedbar-separator-face' are identical (apart from the overline forcibly disabled here)
   `(speedbar-separator-face ((,class (:foreground "black" :background "MYGREEN" :box nil :overline nil))))


   ;; Ggtags
   ;; ------
   `(ggtags-global-line ((,class (:background "MY_TBLU"))))
   ;; TODO: Define 'ggtags-highlight' to customize "highlight tag at point"
   ;; `(ggtags-highlight ((,class ())))


   ;; Markdown
   ;; --------
   ;; Most faces inherit from Font Lock faces and are just fine
   `(markdown-link-face ((,class (:inherit link-visited :bold t))))
   `(markdown-url-face ((,class (:inherit link-visited))))
   `(markdown-header-face-1 ((,class (:foreground "MY__RED"))))
   `(markdown-header-face-2 ((,class (:foreground "MYORNGE"))))
   `(markdown-header-face-3 ((,class (:foreground "MYYELOW"))))
   `(markdown-list-face ((,class (:foreground "MYORNGE"))))
   `(markdown-code-face ((,class (:inherit font-lock-constant-face))))


   ;; Sh-Script
   ;; ---------
   `(sh-heredoc ((,class (:inherit font-lock-constant-face))))

   ;; ReST
   ;; ----
   ;; Different shades of #f78585,
   ;; as suggested on https://www.color-hex.com/color/f78585
   `(rst-level-1 ((,class (:background "#c56a6a"))))
   `(rst-level-2 ((,class (:background "#ac5d5d"))))
   `(rst-level-3 ((,class (:background "#944f4f"))))
   `(rst-level-4 ((,class (:background "#7b4242"))))
   `(rst-level-5 ((,class (:background "#623535"))))
   `(rst-level-6 ((,class (:background "#4a2727"))))
  ))

(custom-theme-set-variables
 'nicedark

 ;; ansi-color-names-vector color order:
 ;;   black, red, green, yellow,
 ;;   blue, magenta, cyan, white
 '(ansi-color-names-vector ["MYBKGND" "MY__RED" "MY_BGRN" "MYYELOW"
                            "MY_BLUE" "MYPRPLE" "MY_CYAN" "MYWHITE"])
 )

(provide-theme 'nicedark)
