What is this?
=============

This is my personal Emacs profile.

The original reasons why I created this repository were to have a backup and to easily synchronize the configuration across the different workstations I use in my daily life.

But since some developers around me showed interest in this configuration, I decided to share it.


Features
========

- Keep original Emacs key bindings as much as possible (beware with CTRL-Z though...)
- Keep a fast loading time
- Bundle pre-configured extensions
- Use Emacs packages when available (transition from discrete extensions to packages is still ongoing)
- Use well sorted init files with a lot of comments to allow readability and easy customization
- Provide a global configuration file to support local machine-specific parameters (e.g. font size, external dependencies...)

Third-party extensions, such as:

- Emacs Code Browser
- Markdown mode
- SR Speedbar
- Xcscope
- Ggtags
- Init-loader
- Magit
- profile dot Emacs
- RFCView
- undo-tree


Installation
============

Basic installation
------------------

1. Delete any existing Emacs initialization script or directory (`~/.emacs`, `~/.emacs.el`, `~/.emacs.d/`...)
2. Clone this repository: `git clone https://konnichy@bitbucket.org/konnichy/emacs.d.git ~/.emacs.d`
3. Customize the installation to suit your local machine by editing `~/.emacs.d/config.el`

`config.el` contains settings that depend on the local system, such as paths to external tools, available fonts, display size, internet access, etc.


Advanced installation
---------------------

Although not mandatory, it is recommended to install a few external tools to take advantage of some advanced features. Emacs configuration scripts related to these tools will detect and initialize them automatically on startup.


### GNU Global

1. Install GNU Global
2. `cd` to your project's root directory
3. (optional) Build the list of files to index: `find . -name '*.[ch]' > gtags.files`
4. Execute the command `gtags` in your project's root directory to launch the first indexation process

On next launch, the Emacs' init scripts should detect the presence of GNU Global on your system and use it instead of etags (Emacs tags), with the same key bindings.
If it doesn't, search for file `gtags.el` on your system (it was installed with GNU Global) and add the path to its directory in `config.el`.

Although usually not necessary because the index database will be updated automatically when saving source files, it is still good to know that the database can be updated manually with the command: `global -u`.


### Cscope

At minimum, install Cscope. When used from Emacs, database file(s) will automatically be created if inexistant. However, if you want to avoid database files being created at several locations inside your project's file hierarchy, it may be advisable to manually create a database at the projet's root directory. Cscope will then use this database and not create new ones as long as it is in a parent directory of files visited.

To make cscope create the database:

1. `cd` to your project's home directory
2. Delete any existing Cscope-related file: `find . -name 'cscope.*' -exec rm -f {} \;`
3. Create the list of files to index. For example with: `find . -type f -name '*.[ch]' > cscope.files`
4. Create the database: `cscope -b -q -k`

Although usually not necessary because the index database will be updated automatically, it is still good to know that the database can be updated manually with the same command as the one used to create it.


Updating
--------

```
cd ~/.emacs.d/
git stash save
git pull
git stash pop
```

Saving modifications in the stash may be necessary if, for example, `config.el` was customized since the first repository installation.


Main files and directories
==========================

- `README.md`: the file you are reading now
- `KEYS.md`: a quick reference to the key bindings I use the most
- `custom.el`: this file is created automatically and used by Emacs to save customization variables
- `my-inits/`: custom initialization files
- `elpa/`: extensions installed via the Emacs' package manager
- `my-extensions/`: extensions installed manually
- `my-themes/`: custom color theme files


TODO-list
=========

- Replace as many 'setq' calls as possible by 'customize-set-variable' calls (explanation here: http://emacs.stackexchange.com/questions/102/advantages-of-setting-variables-with-setq-instead-of-custom-el)
- Clean init files from commented-out code
- Find a front-end to Cscope that looks like the GNU Global's front-end (or the other way around, or open the cscope buffer in the current buffer, or ...)
