This file serves as a quick reference for the most common key bindings I use.
The _default bindings_ are the ones predefined by Emacs or the associated
extension, the _custom bindings_ at the ones defined in
`my-inits/80_key_bindings.el`.


Built-in actions
================

Action                                            | Default binding         | Custom binding
:-------------------------------------------------|:------------------------|:--------------
__Files and buffers__                             |                         |
Open file                                         | `CTRL-x CTRL-f`         |
Save file                                         | `CTRL-x CTRL-s`         |
Exit Emacs                                        | `CTRL-x CTRL-c`         |
Switch buffer                                     | `CTRL-x b`              |
Kill buffer                                       | `CTRL-x k`              |
Open `~/.emacs.d/KEYS.md`                         |                         | `CTRL-x CTRL-k`
__Managing windows__                              |                         |
Split window below                                | `CTRL-x 2`              | (`2` requires `SHIFT` on FR keyboard)
Split window on the right                         | `CTRL-x 3`              | (`3` requires `SHIFT` on FR keyboard)
Cycle through windows                             | `CTRL-x o`              |
Delete the selected window                        | `CTRL-x 0`              | (`0` requires `SHIFT` on FR keyboard)
Delete other windows                              | `CTRL-x 1`              | (`1` requires `SHIFT` on FR keyboard)
Display line numbers in the current window        |                         | `CTRL-l`
__Moving the cursor__                             |                         |
Move the cursor to previous/next word             | `CTRL-LEFT/RIGHT`       |
Move the cursor to previous/next paragraph        | `CTRL-UP/DOWN`          |
Move the cursor to previous/next heading of same level (outline-mode) |     | `META-UP/DOWN`
Move the cursor to previous/next heading (outline-mode, markdown-mode) |    | `SHIFT-META-UP/DOWN`
Go to line number #                               | `META-g g` `META-g META-g` | `META-g`
__Copy/Paste__                                    |                         |
Cut line                                          | `CTRL-k`                |
Cut region                                        | `CTRL-w`                |
Copy region                                       | `CTRL-INSERT`           |
Paste region                                      | `SHIFT-INSERT`          |
__Search/Replace__                                |                         |
Search                                            | `CTRL-s`                |
Search backward                                   | `CTRL-r`                |
Replace                                           | `META-%`                | (`%` requires `SHIFT` on FR keyboard)
__Comments__                                      |                         |
Comment out the region                            | `CTRL-c c`              |
Uncomment the region                              | `CTRL-c u`              |
__Indent/Tabs__                                   |                         |
Indent line                                       | `TAB`                   |
Indent region                                     | `CTRL-META-\`           | (`\` requires `ALTGR` on FR keyboard)
Add a TAB character                               | `CTRL-q TAB`            |
__Undo__                                          |                         |
Undo                                              | `CTRL-_`                | `CTRL-z`
Redo                                              |                         | `CTRL-y`
__Tags__                                          |                         |
Select a TAGS file                                | `META-x visit-tags-table`|
Find tags                                         | `META-.`                |
Go to next match                                  | `CTRL-u META-.`         |
Jump back                                         | `META-*`                | (`.` requires `SHIFT` on FR keyboard)
__Expansion/Completion__                          |                         |
Expansion                                         | `META-/`                |
Completion                                        | `CTRL-META-/`           | (`/` requires `SHIFT` on FR keyboard)
__Other__                                         |                         |
Cancel current action (exit minibuffer)           | `CTRL-g`                |
Read man pages in Emacs (without man)             | `META-x woman`          |
Type in a single shell command                    | `META-!`                |


Custom functions
================

Action                                            | Custom binding
:-------------------------------------------------|:---------------
Highlight all occurences of the selected word     | `CTRL-Q`
Unhighlight all occurences of the selected word   | `SHIFT-CTRL-Q`


Undo-Redo extension
===================

Action                                            | Default binding
:-------------------------------------------------|:---------------
Redo                                              | `META-_`
Visualize the undo tree                           | `CTRL-x u`
__In the undo tree visualization:__               |
Navigate in the undo tree visualizer              | `UP/DOWN/LEFT/RIGHT`
Toggle timestamps                                 | `t`
Quit the undo tree visualizer (accept changes)    | `q`
Quit the undo tree visualizer (cancel changes)    | `CTRL-q`


ECB extension
=============

Action                                            | Default binding
:-------------------------------------------------|:---------------
Activate ECB                                      | `META-x ecb-activate`
Deactivate ECB                                    | `META-x ecb-deactivate`


Magit extension
===============

Action                                            | Default binding
:-------------------------------------------------|:---------------
Open the status buffer                            | `META-x magit-status`
__In the status buffer:__                         |
Display differences in the selected file          | `e` or `d`
Stage the selected modified or untracked file     | `s`
Stage by file directory name                      | `CTRL-u s`
Stage ALL modified and untracked files            | `CTRL-u S`
Unstage the selected file                         | `u`
Unstage ALL staged files                          | `CTRL-u U`
Add the selected file to the ignore list          | `i`
Add a file or directory name to the ignore list   | `CTRL-u i`
Permanently delete a file                         | `k`
Write the commit message                          | `c`
__In the commit message buffer:__                 |
Mark the next commit to amend the current one     | `CTRL-c CTRL-a`
Commit                                            | `CTRL-c CTRL-c`
__From where??__                                  |
Push                                              | `P P`
Pull                                              | `F F`


SR Speedbar extension
=====================

Action                                            | Default binding
:-------------------------------------------------|:---------------
Open the SR Speedbar window                       | `META-x sr-speedbar-open`


RFC View extension
==================

Action                                            | Default binding
:-------------------------------------------------|:---------------
Activate RFC View                                 | `META-x rfcview-mode`
