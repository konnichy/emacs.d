(defconst myconfig-proxy nil
  "Address of the proxy to use for Internet connection.")

;; Source Code Pro
(defconst myconfig-font-family "Source Code Pro"
  "Font style.")
(defconst myconfig-font-height "11pt"
  "Font size.")

;; Ubuntu Mono
;; (defconst myconfig-font-family "Ubuntu Mono"
;;   "Font style.")
;; (defconst myconfig-font-height "11pt"
;;   "Font size.")

(defconst myconfig-coding-style nil
  "Name of the coding style to use.")

(defconst myconfig-ecb-semantic-mode t
  "If t, semantic-mode is activated in ECB. Under some unclear circumstances (too many open files?), this may cause stuttering when used in conjunction with TRAMP.")

;; Note for Debian/Ubuntu users
;; The GNU Global package shipped with your distribution was not updated for years.
;; It is highly recommended that you compile and install GNU Global from sources.
;; If you still use your distribution's precompiled package,
;; please uncomment the two following lines:
;; (defconst myconfig-gtags-load-path "/usr/share/emacs/site-lisp/global"
;;   "Path to 'gtags.el'. If nil, common paths will be tested.")

(defconst myconfig-ggtags-instead-of-gtags t
  "If non-nil, use the ggtags.el frontend to GNU Global instead of the official gtags.el.")
