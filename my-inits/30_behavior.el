;; Disable backup files and autosaves
;; ----------------------------------
(setq make-backup-files nil)
(setq auto-save-default nil)


;; Automatically revert files if changed on disk
;; ---------------------------------------------
(global-auto-revert-mode 1)


;; Parenthesis matching
;; --------------------
(show-paren-mode 1)                    ;; turn paren match highlighting on
(setq show-paren-style 'expression)    ;; highlight entire bracket expression


;; Typing replaces the selection
;; -----------------------------
(delete-selection-mode 1)


;; Mouse wheel
;; -----------
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-scroll-amount (quote (3 ((shift) . 1) ((control)))))


;; Unlock the 'erase-buffer' command
;; ---------------------------------
(put 'erase-buffer 'disabled nil)


;; Enable undo-tree globally
;; -------------------------
(global-undo-tree-mode)
;; Remove the "Undo-Tree" mention on the modeline
(setq undo-tree-mode-lighter "")


;; Use Ido (InteractivelyDoThings) when
;; opening a file or searching a buffer
;; ------------------------------------
(require 'ido)
(add-to-list 'ido-ignore-directories "\.svn")
(add-to-list 'ido-ignore-directories "\.git")
(add-to-list 'ido-ignore-buffers "\\` ")
(add-to-list 'ido-ignore-buffers "^\*scratch\*")
(add-to-list 'ido-ignore-buffers "^\*Mess")
(add-to-list 'ido-ignore-buffers "^\*Back")
(add-to-list 'ido-ignore-buffers ".*Completion")
(add-to-list 'ido-ignore-buffers "^\*Ido")
(add-to-list 'ido-ignore-buffers "^\*trace")
(add-to-list 'ido-ignore-buffers "^\*Buffer-List")
(add-to-list 'ido-ignore-buffers "^\*Compile-Log")  ;; ECB
(add-to-list 'ido-ignore-buffers "^ \*ECB")         ;; ECB (displayed buffers cannot apparently be ignored)
(add-to-list 'ido-ignore-buffers "^global -u")      ;; GNU Global
(add-to-list 'ido-ignore-buffers "^\*SPEEDBAR\*")   ;; Speedbar / Sr Speedbar
(ido-mode t)


;; Copy/Paste and middle mouse click behavior
;; ------------------------------------------
(if (eq system-type 'windows-nt)
    ;; MS Windows
    (progn
      (setq select-active-regions nil)
      (setq mouse-drag-copy-region t)
      (global-set-key [mouse-2] 'mouse-yank-at-click)
      )
  ;; UNIX OSes
  (setq select-active-regions t)
  )
;; Any OS
(setq mouse-drag-copy-region t)
;; (global-set-key [mouse-2] 'mouse-yank-primary)  ;; the default


;; Suppress the command echo in shell mode
;; ---------------------------------------
(when (eq system-type 'windows-nt)
  (setq-default comint-process-echoes 'on))


;; Fix behavior of dead keys
;; -------------------------
;; Under Linux, fix message "<dead circumflex> is undefined"
;; http://unix.stackexchange.com/questions/28170/some-keys-are-invalid-on-emacs-when-using-german-keyboard
(unless (eq system-type 'windows-nt)
  (require 'iso-transl)
)
;; Under MS Windows, the circumflex accent works as a dead key
;; and nothing can be done about that with Emacs initialization files
;; (define-key key-translation-map [dead-circumflex] (lookup-key key-translation-map "\C-x8^"))
;; (define-key key-translation-map [dead-circumflex] "^")


;; Show the Semantic context summary
;; ---------------------------------
;; (variable type, function prototype, ...)
;; --- THE TWO FOLLOWING LINES SHOULD WORK BUT THEY DON'T ---
;; (global-semantic-idle-summary-mode)
;; (customize-set-variable 'global-semantic-idle-summary-mode t)
(add-to-list 'semantic-default-submodes 'global-semantic-idle-summary-mode t)


;; Speedbar
;; --------
;; Don't show images (because they don't match the theme)
(setq speedbar-use-images nil)
;; Don't group identically-prefixed function names together (also affects ECB's Methods buffer)
;; Sort function names in alphabetical order
(setq speedbar-tag-hierarchy-method (quote (speedbar-trim-words-tag-hierarchy speedbar-sort-tag-hierarchy)))
