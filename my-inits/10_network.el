;; Configure the web proxy
;; -----------------------
(if (bound-and-true-p myconfig-proxy)
    (setq url-proxy-services (cons (cons "http" myconfig-proxy) nil))
  )


;; Activate Emacs packages and add more repositories
;; -------------------------------------------------
(when (>= emacs-major-version 24)
  (require 'package)

  ;; ELPA
  ;; ELPA is included by default since Emacs 24

  ;; MELPA
  ;; Setup instructions: https://melpa.org/#/getting-started
  ;; Choose between the default "bleading-edge" and the stable repositories
  (add-to-list 'package-archives
               '("melpa" . "https://melpa.org/packages/") t)
  ;(add-to-list 'package-archives
  ;             '("melpa-stable" . "https://stable.melpa.org/packages/") t)

  (package-initialize)
  )
