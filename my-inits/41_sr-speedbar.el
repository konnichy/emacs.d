;; Customize the size and position of the sr-speedbar window
;; ---------------------------------------------------------
(customize-set-variable 'sr-speedbar-default-width 38)  ;; same width as ECB
(customize-set-variable 'sr-speedbar-right-side nil)


;; Skip sr-speedbar window when cycling windows
;; --------------------------------------------
(customize-set-variable 'sr-speedbar-skip-other-window-p t)


;; Make sr-speedbar open files in the active window
;; ------------------------------------------------
;; The current version of sr-speedbar opens selected files in the "previous" window.
;; The problem is that the order of windows in the Emacs' list of windows is not
;; related to the order they are selected, but to the order of their creation.
;; Therefore, sr-speedbar always opens selected files in the same window instead
;; of the currently active one.
;; NOTE: This is a dirty and partial fix but it works in most of the cases. This fix
;; relies on the fact that contrary to the windows' list, the buffers' list is
;; reordered by Emacs every time a different buffer is visited.
(defun my-previous-window ()
  "Return the window which was selected before the currently selected one."
  (catch 'found
    (dolist (buf (buffer-list))
      (setq bufname (buffer-name buf))
      (when (not (string= bufname sr-speedbar-buffer-name))
        (throw 'found (get-buffer-window bufname)))))
)
(defun my-select-previous-window ()
  "Select the previous window"
  (select-window (my-previous-window)))
(defun my-fix-speedbar-hooks ()
  "Function fixing speedbar hooks to solve sr-speedbar issues."
  ;; Remove hooks added by sr-speedbar
  (setq speedbar-before-visiting-file-hook nil)
  (setq speedbar-before-visiting-tag-hook nil)
  (setq speedbar-visiting-file-hook nil)
  (setq speedbar-visiting-tag-hook nil)
  ;; Add custom hooks
  (add-hook 'speedbar-before-visiting-file-hook 'my-select-previous-window t)
  (add-hook 'speedbar-before-visiting-tag-hook 'my-select-previous-window t)
  ;; TODO: Re-select the window after the buffer switch
  ;; (add-hook 'speedbar-visiting-file-hook 'previous-window t)
  ;; (add-hook 'speedbar-visiting-tag-hook 'previous-window t)
)

;; NOTE: 'my-fix-speedbar-hooks' should be used as a hook or advice when opening
;; sr-speedbar but all my attempts failed. This is why I chose to open sr-speedbar,
;; call this function to fix the hooks, then close sr-speedbar before the user notices.
(sr-speedbar-open)

;; Also add some more file types to support
(speedbar-add-supported-extension ".md")
(speedbar-add-supported-extension ".markdown")
(speedbar-add-supported-extension ".css")
(speedbar-add-supported-extension ".json")
(speedbar-add-supported-extension ".sh")

(my-fix-speedbar-hooks)
(sr-speedbar-close)


;; --- TESTING ---

;; Avoid the SR Speedbar window to be affected when deleting other windows
;; Source: http://www.emacswiki.org/emacs/SrSpeedbar
;; -----------------------------------------------------------------------
;; (defadvice delete-other-windows (after my-sr-speedbar-delete-other-window-advice activate)
;;   "Check whether we are in speedbar, if it is, jump to next window."
;;   (let ()
;; 	(when (and (sr-speedbar-window-exist-p sr-speedbar-window)
;;                (eq sr-speedbar-window (selected-window)))
;;       (other-window 1)
;;       )))
;; (ad-enable-advice 'delete-other-windows 'after 'my-sr-speedbar-delete-other-window-advice)
;; (ad-activate 'delete-other-windows)


;; Keep speed bar window width after resizing the frame
;; (must be executed after 'sr-speedbar-open')
;; Source: http://www.emacswiki.org/emacs/SrSpeedbar
;; ----------------------------------------------------
;; (sr-speedbar-open)
;; (with-current-buffer sr-speedbar-buffer-name
;;   (setq window-size-fixed 'width))

;; Make Sr-speedbar open files in the next window,
;; instead of in the previous window
;; -----------------------------------------------
;; (defun select-next-window ()
;;   (other-window 1))
;; (defun my-sr-speedbar-open-hook ()
;;   (add-hook 'speedbar-before-visiting-file-hook 'select-next-window t)
;;   (add-hook 'speedbar-before-visiting-tag-hook 'select-next-window t)
;;   )
;; (advice-add 'sr-speedbar-open :after #'my-sr-speedbar-open-hook)
