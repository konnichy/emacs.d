(when (eq system-type 'windows-nt)

  ;; Set the path to AutoHotKey's extras
  ;; -----------------------------------
  (setq autohotkey-editors-path (concat (getenv "ProgramFiles(x86)") "\\AutoHotKey\\Extras\\Editors"))

  ;; If AutoHotKey is actually installed...
  ;; --------------------------------------
  (when (file-exists-p (concat autohotkey-editors-path "\\Emacs\\ahk-mode.el"))

    ;; Set the load path
    ;; -----------------
    (add-to-list 'load-path (concat autohotkey-editors-path "\\Emacs"))

    ;; Initialize as described in 'ahk-mode.el'
    ;; ----------------------------------------
    (setq ahk-syntax-directory (concat autohotkey-editors-path "\\Syntax"))
    (add-to-list 'auto-mode-alist '("\\.ahk$" . ahk-mode))
    (autoload 'ahk-mode "ahk-mode")
    )
  )
