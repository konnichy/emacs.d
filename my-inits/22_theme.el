;; Load the Emacs24-compatible theme
;; ---------------------------------
(when (>= emacs-major-version 24)
  ;; Add the custom themes directory to the themes load path
  ;; -------------------------------------------------------
  (add-to-list 'custom-theme-load-path "~/.emacs.d/my-themes")

  ;; Load my theme
  ;; -------------
  (if window-system
      (load-theme 'nicedark t)
    (load-theme 'nicedark-term t)
    )
)
