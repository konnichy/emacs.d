;; Do not show the welcome buffer
;; ------------------------------
(setq inhibit-splash-screen t)


;; Set the default frame's dimensions
;; ----------------------------------
;; Read for "standard" (historic) values:
;; http://en.wikipedia.org/wiki/Characters_per_line
(add-to-list 'default-frame-alist '(width . 90))
(add-to-list 'default-frame-alist '(height . 44))


;; Hide the tool bar
;; -----------------
(tool-bar-mode -1)


;; Hide the scoll bars
;; -------------------
(scroll-bar-mode -1)


;; Display column number in the mode line
;; --------------------------------------
(column-number-mode t)


;; Select the font family
;; ----------------------
(set-face-attribute 'default nil :family myconfig-font-family)


;; Select the font size
;; --------------------
(when (string= myconfig-font-height "10pt")
  (set-face-attribute 'default nil :height 98))
(when (string= myconfig-font-height "10.5pt")
  (set-face-attribute 'default nil :height 105))
(when (string= myconfig-font-height "11pt")
  (set-face-attribute 'default nil :height 113))
(when (string= myconfig-font-height "12pt")
  (set-face-attribute 'default nil :height 120))


;; Consider UTF-8 by default
;; -------------------------
(if (eq system-type 'windows-nt)
    (prefer-coding-system 'utf-8))


;; In case of Nyan-mode, shorten the length
;; ----------------------------------------
(setq nyan-bar-length 20)


;; Highlight useless whitespaces and long lines
;; --------------------------------------------
;; Source:
;; https://www.emacswiki.org/emacs/HighlightLongLines
(customize-set-variable 'global-whitespace-mode t)
(customize-set-variable 'whitespace-line-column 80)
(customize-set-variable 'whitespace-style (quote (face trailing lines-tail empty indentation::space)))
