;; Start the Emacs Server to allow reusing a single instance of Emacs
;; ------------------------------------------------------------------
;; Usage:
;;   emacsclient -n <file>   : Open a new buffer for file in a running instance of Emacs
;;   emacsclient -nc <file>  : Open the file in a new frame of a running instance of a graphical Emacs
;;   emacsclient -nw <file>  : Open the file in a new console instance of Emacs
(require 'server)
(unless (server-running-p)
  (server-start)
  )
