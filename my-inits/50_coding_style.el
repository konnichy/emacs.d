;; DEFAULT: Indentation style to 4 *spaces*
;; ---------------------------------------
(setq-default indent-tabs-mode nil)    ;; disable indentation with tabs
(setq-default tab-width 4)             ;; used when DISPLAYING tabs
(setq-default tab-stop-list (number-sequence 4 200 4))  ;; used when ADDING tabs
(setq-default c-basic-offset 4)        ;; for C-mode


;; Define the style used at NetASQ
;; -------------------------------
(when (string= myconfig-coding-style "netasq")

  (defconst netasq-c-style
    ;; Always indent c/c++ sources, never insert tabs
    '(
      (indent-tabs-mode           . t)
      (c-tab-always-indent        . t)
      ;; Offset for line only comments
      ;; (c-comment-only-line-offset . 0)
      ;; Controls the insertion of newlines before and after braces.
      ;; default is (before after)
      (c-hanging-braces-alist     . (
                                        ; if ( ) {  }
                                        ;        ^  ^
                                     (substatement-open  . (before after))
                                     (substatement-close . (before after))
                                        ; void foo ( ) { .. }
                                        ;              ^    ^
                                     (defun-open . (before after))
                                     (defun-close . (before after))
                                        ;  { .. }
                                        ;  ^    ^
                                     (block-open  . (before after))
                                     (block-close . (before after))
                                        ; static char gni[] = { .. }
                                        ;                     ^    ^
                                     (brace-list-open  . (before after))
                                     (brace-list-close . (before after))
                                        ; case label: { .. }
                                        ;             ^    ^
                                     (statement-case-open  . (before after))
                                     (statement-case-close . (before after))
                                        ; extern "C" { .. }
                                        ;            ^    ^
                                     (extern-lang-open  . (before after))
                                     (extern-lang-close . (before after))
                                     )
                                  )
      ;; Controls the insertion of newlines before and after certain colons.
      (c-hanging-colons-alist     . ((member-init-intro before)
                                     (inher-intro)
                                     (case-label after)
                                     (label after)
                                     (access-label after)))
      ;; List of various C/C++/ObjC constructs to "clean up".
      (c-cleanup-list             . (scope-operator))
      ;; Association list of syntactic element symbols and indentation offsets.
                                        ; +   c-basic-offset times 1
                                        ; -   c-basic-offset times -1
                                        ; ++  c-basic-offset times 2
                                        ; --  c-basic-offset times -2
                                        ; *   c-basic-offset times 0.5
                                        ; /   c-basic-offset times -0.5
      (c-offsets-alist            . (
                                     (comment-intro     . 0)
                                     (statement         . 0)
                                     (substatement-open . 0)
                                     (case-label        . +)
                                     (block-open        . 0)
                                        ;(label             . 0)
                                     (arglist-cont-nonempty . +)
                                     (statement-case-intro . 0)
                                     )
                                  )
                                        ; (c-echo-syntactic-information-p . t)
      )
    "C/C++ programming style at NetASQ")
  (c-add-style "netasq" netasq-c-style)
  (defun netasq-c-mode-hook () (c-set-style "netasq"))

  ;; --- MAKE SURE THE NETASQ CODING STYLE IS ENABLED AFTER GTAGS ---
  (add-hook 'c-mode-hook 'netasq-c-mode-hook)
  (add-hook 'c++-mode-hook 'netasq-c-mode-hook)

  )
