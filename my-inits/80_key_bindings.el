;; Open KEYS.md
(global-set-key (kbd "C-x C-k") (lambda() (interactive) (find-file (expand-file-name "KEYS.md" user-emacs-directory))))

;; CTRL-l Line numbers in the left margin of the current window
(global-set-key [?\C-l] 'linum-mode)

;; CTRL-z Undo
(global-set-key (kbd "C-z") 'undo)
;; CTRL-y Redo
(global-set-key (kbd "C-y") 'redo)

;; CTRL-a Select all
;(global-set-key "\C-a" 'mark-whole-buffer)

;; CTRL-c + c Comment region
(global-set-key "\C-cc" 'comment-region)
;; CTRL-c + u Uncomment region
(global-set-key "\C-cu" 'uncomment-region)

;; META-UP Previous heading (outline-mode, markdown-mode)
(global-set-key [(shift meta up)] 'outline-previous-visible-heading)
(global-set-key [(meta up)] 'outline-backward-same-level)
;; META-DOWN Next heading (outline-mode, markdown-mode)
(global-set-key [(shift meta down)] 'outline-next-visible-heading)
(global-set-key [(meta down)] 'outline-forward-same-level)

;; META-g Go to line (in addition to the default META-g META-g and META-g g)
(global-set-key "\M-g" 'goto-line)

;; CTRL-META-. Find a symbol with GNU Global (instead of a pattern)
(global-set-key (kbd "C-M-.") 'gtags-find-symbol)


;; Compilation and debugging
;; -------------------------
(global-set-key (kbd "<f5>") 'recompile)
(global-set-key (kbd "<S-f5>") 'compile)
(global-set-key (kbd "<f6>") 'next-error)
(global-set-key (kbd "<S-f6>") 'previous-error)
(global-set-key (kbd "<f7>") 'gud-gdb)
(global-set-key (kbd "<S-f7>") 'gdb)


;; For French keyboards, choose either one
;; ---------------------------------------
;; 1| Change complicated bindings to their intended position on a QWERTY keyboard
;; META-% becomes META-�
;; META-. becomes META-:
;; META-/ becomes META-!
;; CTRL-META-/ becomes CTRL-META-!
;; 2| Remove the need for the SHIFT key
;; META-% becomes META-�
;; META-. becomes META-;
;; META-/ becomes META-:
;; CTRL-META-/ becomes CTRL-META-:


;; Custom functions from `72_functions.el`
;; ---------------------------------------
(global-set-key (kbd "C-q") 'my-highlight-region)
(global-set-key (kbd "C-S-q") 'my-unhighlight-region)
