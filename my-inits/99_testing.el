;; Hide passwords when using Emacs' shell
;; --------------------------------------
;; (or manually use 'M-x send-invisible' before typing a password)
;(add-hook 'comint-output-filter-functions 'comint-watch-for-password-prompt)
;; (setq comint-password-prompt-regexp
;;       (concat
;;        "\\("
;;        comint-password-prompt-regexp
;;        "\\)\\|Enter PEM pass phrase:"))


;; works locally and through TRAMP
;; (setq compile-command "sudo chroot /chroot/9.3-64 su -l ${USER} -c '. ~/.gitfwrc ; make -C\${NETASQ_TMP}/crossplatform/src/ASQ/asqcheck/ asqcheck'")

(setq compile-command "
git_dir=$(git rev-parse --show-toplevel 2> /dev/null)
if [ $? -eq 0 ]; then
  make -C ${git_dir}/dev
else
  make
fi
")

;; (message "(display-pixel-width) = %d" (display-pixel-width))
;; (message "(display-pixel-height) = %d" (display-pixel-height))


;; Fullscreen
;; (first frame only)
;; ------------------
;; (customize-set-variable 'initial-frame-alist (quote ((fullscreen . maximized))))


;; Get display dimensions
;; ----------------------
;; (setq display-dpi
;;       (string-to-number
;;        (let ((xdpyinfo
;;               (with-output-to-string
;;                 (call-process "xdpyinfo" nil standard-output))))
;;          (string-match "resolution: +\\(.+\\)x.+ dots per inch" xdpyinfo)
;;          (match-string 1 xdpyinfo))))
;; ;(message "resolution: %d dpi" display-dpi)
;; (setq display-width (/ (display-pixel-width) display-dpi))
;; (setq display-height (/ (display-pixel-height) display-dpi))


;; Function to display ANSI color codes in files
;; ---------------------------------------------
(require 'ansi-color)
(defun display-ansi-colors ()
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))


;; Attempts to maximize Emacs' X window
;; ------------------------------------
;; (set-frame-position (selected-frame) 0 0)
;; (set-frame-size (selected-frame) 400 400)


;; Prepare my development setup
;; ----------------------------
;; (package 'wmctrl' must be installed on the host)
(defun my-dev ()
  (interactive)
  (if (not (eq system-type 'windows-nt))
      ;; MS Windows
      (shell-command "wmctrl -r :ACTIVE: -btoggle,fullscreen")
    )
  (sleep-for 1)  ;; TODO: find another way
  (sr-speedbar-open)
  (other-window 1)
  (split-window-right)
  )
