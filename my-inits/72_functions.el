;; Highlight all occurences of the selected text
;; ---------------------------------------------
(defun my-highlight-region ()
  "Function to highlight all occurences of the selected region in the buffer."
  (interactive)
  (highlight-regexp (buffer-substring-no-properties (region-beginning) (region-end)) 'hi-blue)
)
(defun my-unhighlight-region ()
  "Function to unhighlight all occurences of the selected region in the buffer."
  (interactive)
  (unhighlight-regexp (buffer-substring-no-properties (region-beginning) (region-end)))
)
