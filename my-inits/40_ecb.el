;; WHEN UPGRADING TO A NEW VERSION:
;; If 'ecb-autoloads.el' does not exist in the extension's directory,
;; preparation below will (silently?) fail and 'ecb-activate' will not
;; be recognized. To generate 'ecb-autoloads.el', please:
;; - switch the two 'require' lines below to activate ECB when Emacs loads
;; - restart Emacs
;; - type: META-x ecb-update-autoloads <RET>
;; - switch the two 'require' lines below again to only load ECB on activation
;; - restart Emacs and try: META-x ecb-activate <RET>


;; Prepare (or load) ECB
;; ---------------------
;; (require 'ecb)  ;; this loads ECB when Emacs loads
(require 'ecb-autoloads)  ;; this loads ECB on activation


;; --- WHAT WAS THIS FOR? ---
;; (setq stack-trace-on-error t)
;; ^ I don't what this is supposed to do, but it is generally needed
;; if ECB doesn't work at all. However, with the current version I use,
;; this is apparently not needed.


;; --------------------------------------------------------------------
;; --- BEGIN INITIALIZATION STEPS TO PERFORM AT ECB ACTIVATION TIME ---
;; --------------------------------------------------------------------
(add-hook 'ecb-before-activate-hook (lambda()


;; Disable the "Tip of the day" popup message
;; ------------------------------------------
(setq ecb-tip-of-the-day nil)


;; Show source files in the ECB-directories buffer
;; -----------------------------------------------
(setq ecb-show-sources-in-directories-buffer 'always)


;; Only show method names in the ECB-methods buffer
;; (not the parameters, not the return type)
;; ------------------------------------------------
(customize-set-variable 'ecb-tag-display-function '((default . ecb-format-tag-abbreviate)))


;; Hide the window number in all ECB buffers
;; -----------------------------------------
(customize-set-variable 'ecb-mode-line-display-window-number nil)


;; Create a custom layout
;; ----------------------
;; (for help, read http://ecb.sourceforge.net/docs/Programming-a-new-layout.html
;; or use the wizard with 'ecb-create-new-layout' and see '~/.ecb-user-layouts.el')
;; (this function REALLY NEEDS to be called via a hook when using ecb-autoloads)
(ecb-layout-define
    "my-ecb-layout" left nil
    ;; The frame is already splitted side-by-side and point stays in the
    ;; left window (= the ECB-tree-window-column)

    ;; Here is the creation code for the new layout

    ;; - Defining the current window/buffer as ECB-directories buffer
    (ecb-set-directories-buffer)
    ;; - Splitting the ECB-tree-windows-column in two windows
    (ecb-split-ver 0.25 t)
    ;; - Go to the second window
    (other-window 1)

    ;; --- THIS IS COMMENTED OUT BECAUSE ecb-show-sources-in-directories-buffer IS ENABLED ---
    ;; ;; - Defining the current window/buffer as ECB-sources buffer
    ;; (ecb-set-sources-buffer)
    ;; ;; - Splitting the ECB-tree-windows-column in two windows
    ;; (ecb-split-ver 0.33 t)
    ;; ;; - Go to the second window
    ;; (other-window 1)

    ;; - Defining the current window/buffer as ECB-methods buffer
    (ecb-set-methods-buffer)
    ;; - Splitting the ECB-tree-windows-column in two windows
    (ecb-split-ver 0.67 t)
    ;; - Go to the second window
    (other-window 1)

    ;; - Defining the current window/buffer as ECB-history buffer
    (ecb-set-history-buffer)

    ;; - Make the ECB-edit-window current (see Postcondition above)
    (select-window (next-window))
)


;; Customize the size of ECB-windows
;; (all layouts)
;; ---------------------------------
;; To get a precise value:
;; 1. Manually drag windows' borders where desired
;; 2. Save the layout with the command 'ecb-store-window-sizes'
;; 3. Check the width value in 'init.el'
;; 4. Modify this value below
;; 5. Remove the line starting with 'ecb-layout-window-sizes' from 'init.el'
(setq ecb-windows-width 0.16346153846153846)


;; Customize the size of ECB-windows
;; (per layout)
;; ---------------------------------
;; (setq ecb-layout-window-sizes
;;       (quote (("left8"
;;                (ecb-directories-buffer-name 0.2 . 0.25)
;;                (ecb-sources-buffer-name 0.2 . 0.25)
;;                (ecb-methods-buffer-name 0.2 . 0.35)
;;                (ecb-history-buffer-name 0.2 . 0.15)))))
;; (setq ecb-layout-window-sizes
;;       (quote (("left3"
;;                (0.2 . 0.25)
;;                (0.2 . 0.25)
;;                (0.2 . 0.50)))))
;; (setq ecb-layout-window-sizes
;;       (quote (("my-ecb-layout"
;;                (ecb-directories-buffer-name 0.2 . 0.25)
;;                (ecb-sources-buffer-name 0.2 . 0.25)
;;                (ecb-methods-buffer-name 0.2 . 0.25)
;;                (ecb-history-buffer-name 0.2 . 0.25)))))


;; Select the new layout
;; ---------------------
(setq ecb-layout-name "my-ecb-layout")


;; Enable Semantic
;; ---------------
;; (this allows to:
;; - sort ECB-Methods buffer by name
;; - highlight the method in which the cursor points
;; - and more...)
;; NOTE:
;; If this line causes the error 'Wrong type argument , stringp, 1',
;; this is likely because g++ is not installed on the system
;; Source: http://stackoverflow.com/questions/14214714/cedet-wrong-type-argument-stringp-1
(if (bound-and-true-p myconfig-ecb-semantic-mode)
    (semantic-mode))


))
;; ------------------------------------------------------------------
;; --- END INITIALIZATION STEPS TO PERFORM AT ECB ACTIVATION TIME ---
;; ------------------------------------------------------------------
