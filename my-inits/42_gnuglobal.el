;; Set the path to GNU Global
;; --------------------------
;; Default location on BSD
(if (eq system-type 'berkeley-unix)
    (setq gtags-load-path "/usr/local/share/gtags"))

;; Default location on GNU/Linux (installation from sources)
(if (eq system-type 'gnu/linux)
    (setq gtags-load-path "/usr/local/share/gtags"))
    ;; old path (still in use under Debian/Ubuntu):
    ;; (setq gtags-load-path "/usr/share/emacs/site-lisp/global"))

;; Default location on MS Windows
(if (eq system-type 'windows-nt)
    (setq gtags-load-path "C:\\Program Files (x86)\\Global\\share\\gtags"))

;; If a path was manually defined, overwrite the default location
(if (bound-and-true-p myconfig-gtags-load-path)
    (setq gtags-load-path myconfig-gtags-load-path))


;; If the package is actually installed, load and enable GNU Global
;; ----------------------------------------------------------------
(when (file-exists-p (concat gtags-load-path "/gtags.el"))

  ;; Load gtags when entering gtags-mode
  ;; -----------------------------------
  (add-to-list 'load-path gtags-load-path)
  (autoload 'gtags-mode "gtags" "" t)

  ;; Enter gtags-mode with main supported languages
  ;; ----------------------------------------------
  (add-hook 'c-mode-hook (gtags-mode t))
  (add-hook 'c++-mode-hook (gtags-mode t))
  (add-hook 'java-mode-hook (gtags-mode t))

  ;; Replace the regular etags key bindings with their gtags equivalent
  ;; ------------------------------------------------------------------
  (global-set-key (kbd "M-.") 'gtags-find-tag)
  (global-set-key (kbd "M-*") 'gtags-pop-stack)
  (global-set-key (kbd "C-M-.") 'gtags-find-pattern)

  ;; Automatically update tags when saving a file
  ;; --------------------------------------------
  (setq gtags-auto-update t)

  ;; Set the path to client scripts for remote execution with TRAMP
  ;; --------------------------------------------------------------
  (add-to-list 'exec-path (concat gtags-load-path "/script/"))
  )


;; If ggtags is installed and prefered...
;; --------------------------------------
(when (and (fboundp 'ggtags-find-tag-dwim) myconfig-ggtags-instead-of-gtags)

  ;; Replace the regular etags key bindings with their ggtags equivalent
  ;; -------------------------------------------------------------------
  (global-set-key (kbd "M-.") 'ggtags-find-tag-dwim)
  (global-set-key (kbd "M-*") 'ggtags-prev-mark)
  (global-set-key (kbd "C-M-.") 'ggtags-find-tag-regex)

  ;; If GNU Global is >=6.5, enable sorting by proximity
  ;; ---------------------------------------------------
  (let (global-version global-version-number)
    ;; Retrieve GNU Global's version
    (setq global-version (shell-command-to-string (concat gtags-global-command " --version")))
    (string-match ".*(GNU GLOBAL) \\(.*\\)" global-version)
    (setq global-version-number (split-string (match-string 1 global-version) "\\."))
    (let (
          (major (string-to-number (car global-version-number)))
          (minor (string-to-number (car (cdr global-version-number))))
          )
      ;; Check that the version is at least 6.5
      (if (or (> major 6) (and (= major 6) (>= minor 5)))
          ;; Enable sorting of GNU Global results by proximity of the reference tag
          (setq ggtags-sort-by-nearness t)
        )
      )
    )
  )
